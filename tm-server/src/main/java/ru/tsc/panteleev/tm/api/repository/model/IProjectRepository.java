package ru.tsc.panteleev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    void set(@NotNull Collection<Project> projects);

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<Project> findAll();

    @Nullable
    Project findById(@NotNull String userId, @NotNull String id);

    void removeById(@NotNull String userId, @NotNull String id);

    void clearByUserId(@NotNull String userId);

    void clear();

    long getSize(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

}
