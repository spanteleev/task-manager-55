package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ISenderService {

    void send(@NotNull String message);

}
