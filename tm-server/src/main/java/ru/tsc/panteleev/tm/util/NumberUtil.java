package ru.tsc.panteleev.tm.util;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;

public interface NumberUtil {

    @NotNull
    static String formatBytes(final long bytes) {
        @NotNull final DecimalFormat decimalFormat = new DecimalFormat("#.###");
        @NotNull final String[] units = {"B", "KB", "MB", "GB", "TB", "PB", "EB"};
        short unit = 0;
        double convertValue = bytes;
        while (convertValue >= 1024 && unit < units.length - 1) {
            convertValue /= 1024;
            unit++;
        }
        return decimalFormat.format(convertValue) + " " + units[unit];
    }
}
