package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.panteleev.tm.api.service.IConnectionService;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.panteleev.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.panteleev.tm.dto.model.ProjectDTO;
import ru.tsc.panteleev.tm.dto.model.UserDTO;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.service.dto.ProjectServiceDTO;
import ru.tsc.panteleev.tm.service.dto.UserServiceDTO;

import java.util.*;

public class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @NotNull
    private final IUserServiceDTO userService = new UserServiceDTO(connectionService, propertyService);

    @NotNull
    private final UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());

    @NotNull
    private final static String STRING_RANDOM = UUID.randomUUID().toString();

    @NotNull
    private final static String STRING_EMPTY = "";

    private final int COUNT_TEST_PROJECT = 100;

    @After
    public void end() {
        projectService.clear(user.getId());
    }

    public void addTestRecords() {
        for (int i = 0; i < COUNT_TEST_PROJECT; i++)
            projectService.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null);
    }

    @Test
    public void findAll() {
        Assert.assertEquals(0, projectService.getSize(user.getId()));
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_PROJECT, projectService.getSize(user.getId()));
        Assert.assertEquals(COUNT_TEST_PROJECT, projectService.findAll(user.getId()).size());
    }

    @Test
    public void findById() {
        Assert.assertEquals(0, projectService.getSize(user.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.findById(STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.findById(STRING_RANDOM, STRING_EMPTY));
        ProjectDTO project =
                projectService.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null);
        Assert.assertNotNull(projectService.findById(user.getId(), project.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.findById(user.getId(), STRING_RANDOM));
    }

    @Test
    public void removeById() {
        @NotNull final ProjectDTO project =
                projectService.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null);
        Assert.assertNotNull(projectService.findById(user.getId(), project.getId()));
        projectService.removeById(user.getId(), project.getId());
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.findById(user.getId(), project.getId()));
    }

    @Test
    public void clear() {
        addTestRecords();
        projectService.clear(user.getId());
        Assert.assertEquals(0, projectService.getSize(user.getId()));
    }

    @Test
    public void existsById() {
        @NotNull final ProjectDTO project =
                projectService.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null);
        Assert.assertFalse(projectService.existsById(user.getId(), STRING_RANDOM));
        Assert.assertTrue(projectService.existsById(user.getId(), project.getId()));
    }

    @Test
    public void getSize() {
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_PROJECT, projectService.getSize(user.getId()));
    }

    @Test
    public void create() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final ProjectDTO project =
                projectService.create(user.getId(), name, description, null, null);
        @NotNull final ProjectDTO projectAfterCreate = projectService.findById(user.getId(), project.getId());
        Assert.assertNotNull(projectAfterCreate);
        Assert.assertSame(name, projectAfterCreate.getName());
        Assert.assertSame(description, projectAfterCreate.getDescription());
    }

    @Test
    public void updateById() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final ProjectDTO project =
                projectService.create(user.getId(), name, description, null, null);
        @NotNull final String updateName = UUID.randomUUID().toString();
        @NotNull final String updateDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(projectService.updateById(user.getId(), project.getId(), updateName, updateDescription));
        @NotNull final ProjectDTO projectAfterUpdate = projectService.findById(user.getId(), project.getId());
        Assert.assertSame(updateName, projectAfterUpdate.getName());
        Assert.assertSame(updateDescription, projectAfterUpdate.getDescription());
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectService.updateById(STRING_EMPTY, project.getId(), STRING_RANDOM, STRING_RANDOM));
        Assert.assertThrows(IdEmptyException.class,
                () -> projectService.updateById(user.getId(), STRING_EMPTY, STRING_RANDOM, STRING_RANDOM));
    }

    @Test
    public void changeStatusById() {
        @NotNull final ProjectDTO project =
                projectService.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null);
        Assert.assertSame(Status.NOT_STARTED, project.getStatus());
        projectService.changeStatusById(user.getId(), project.getId(), Status.IN_PROGRESS);
        @NotNull ProjectDTO projectAfterUpdate = projectService.findById(user.getId(), project.getId());
        Assert.assertSame(Status.IN_PROGRESS, projectAfterUpdate.getStatus());
        projectService.changeStatusById(user.getId(), projectAfterUpdate.getId(), Status.COMPLETED);
        projectAfterUpdate = projectService.findById(user.getId(), project.getId());
        Assert.assertSame(Status.COMPLETED, projectAfterUpdate.getStatus());
    }

}
