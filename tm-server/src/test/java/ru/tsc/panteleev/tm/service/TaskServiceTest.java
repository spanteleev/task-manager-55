package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.panteleev.tm.api.service.IConnectionService;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.panteleev.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;
import ru.tsc.panteleev.tm.dto.model.UserDTO;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.service.dto.TaskServiceDTO;
import ru.tsc.panteleev.tm.service.dto.UserServiceDTO;

import java.util.UUID;

public class TaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @NotNull
    private final IUserServiceDTO userService = new UserServiceDTO(connectionService, propertyService);

    @NotNull
    private final UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());

    @NotNull
    private final static String STRING_RANDOM = UUID.randomUUID().toString();

    @NotNull
    private final static String STRING_EMPTY = "";

    private final int COUNT_TEST_TASK = 100;

    @After
    public void end() {
        taskService.clear(user.getId());
    }

    public void addTestRecords() {
        for (int i = 0; i < COUNT_TEST_TASK; i++)
            taskService.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null);
    }

    @Test
    public void findAll() {
        Assert.assertEquals(0, taskService.getSize(user.getId()));
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_TASK, taskService.getSize(user.getId()));
        Assert.assertEquals(COUNT_TEST_TASK, taskService.findAll(user.getId()).size());
    }

    @Test
    public void findById() {
        Assert.assertEquals(0, taskService.getSize(user.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.findById(STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.findById(STRING_RANDOM, STRING_EMPTY));
        @NotNull final TaskDTO task = taskService.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null);
        Assert.assertNotNull(taskService.findById(user.getId(), task.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.findById(user.getId(), STRING_RANDOM));
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, taskService.getSize(user.getId()));
        @NotNull final TaskDTO task = taskService.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null);
        Assert.assertNotNull(taskService.findById(user.getId(), task.getId()));
        taskService.removeById(user.getId(), task.getId());
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.findById(user.getId(), task.getId()));
    }

    @Test
    public void clear() {
        addTestRecords();
        taskService.clear(user.getId());
        Assert.assertEquals(0, taskService.getSize(user.getId()));
    }

    @Test
    public void existsById() {
        @NotNull final TaskDTO task = taskService.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null);
        Assert.assertFalse(taskService.existsById(user.getId(), STRING_RANDOM));
        Assert.assertTrue(taskService.existsById(user.getId(), task.getId()));
    }

    @Test
    public void getSize() {
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_TASK, taskService.getSize(user.getId()));
    }

    @Test
    public void create() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final TaskDTO task = taskService.create(user.getId(), name, description, null, null);
        @NotNull final TaskDTO taskAfterCreate = taskService.findById(user.getId(), task.getId());
        Assert.assertNotNull(taskAfterCreate);
        Assert.assertSame(name, taskAfterCreate.getName());
        Assert.assertSame(description, taskAfterCreate.getDescription());
        Assert.assertThrows(UserIdEmptyException.class,
                () -> taskService.create(STRING_EMPTY, STRING_RANDOM, STRING_RANDOM, null, null));
        Assert.assertThrows(NameEmptyException.class,
                () -> taskService.create(user.getId(), STRING_EMPTY, STRING_RANDOM, null, null));
    }

    @Test
    public void updateById() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final TaskDTO task = taskService.create(user.getId(), name, description, null, null);
        @NotNull final String updateName = UUID.randomUUID().toString();
        @NotNull final String updateDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(taskService.updateById(user.getId(), task.getId(), updateName, updateDescription));
        @NotNull final TaskDTO taskAfterUpdate = taskService.findById(user.getId(), task.getId());
        Assert.assertSame(updateName, taskAfterUpdate.getName());
        Assert.assertSame(updateDescription, taskAfterUpdate.getDescription());
        Assert.assertThrows(UserIdEmptyException.class,
                () -> taskService.updateById(STRING_EMPTY, task.getId(), STRING_RANDOM, STRING_RANDOM));
        Assert.assertThrows(IdEmptyException.class,
                () -> taskService.updateById(user.getId(), STRING_EMPTY, STRING_RANDOM, STRING_RANDOM));
    }

    @Test
    public void changeStatusById() {
        @NotNull final TaskDTO task = taskService.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null);
        Assert.assertSame(Status.NOT_STARTED, task.getStatus());
        taskService.changeStatusById(user.getId(), task.getId(), Status.IN_PROGRESS);
        @NotNull TaskDTO taskAfterUpdate = taskService.findById(user.getId(), task.getId());
        Assert.assertSame(Status.IN_PROGRESS, taskAfterUpdate.getStatus());
        taskService.changeStatusById(user.getId(), task.getId(), Status.COMPLETED);
        taskAfterUpdate = taskService.findById(user.getId(), task.getId());
        Assert.assertSame(Status.COMPLETED, taskAfterUpdate.getStatus());
    }

}
