package ru.tsc.panteleev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.tsc.panteleev.tm.util.TerminalUtil;

@Component
public class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskEndpoint().completeTaskStatusById(new TaskCompleteByIdRequest(getToken(), id));
    }

}
