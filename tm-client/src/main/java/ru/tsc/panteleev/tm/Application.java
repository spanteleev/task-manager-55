package ru.tsc.panteleev.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.panteleev.tm.component.Bootstrap;
import ru.tsc.panteleev.tm.configuration.ClientConfiguration;

public class Application {

    public static void main(String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
