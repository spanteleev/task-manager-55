package ru.tsc.panteleev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.api.service.*;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.panteleev.tm.util.SystemUtil;
import ru.tsc.panteleev.tm.util.TerminalUtil;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap {

    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    public void initCommands(@Nullable final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) return;
            commandService.add(command);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void runStartupOperation() {
        initPID();
        initCommands(abstractCommands);
        fileScanner.start();
        registryShutdownHookOperation();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
    }

    private void registryShutdownHookOperation() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
                fileScanner.stop();
            }
        });
    }

    public void run(@Nullable String[] args) {
        if (runWithArgument(args))
            System.exit(0);
        runStartupOperation();
        while (true)
            runWithCommand();
    }

    public void runWithCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            String command = TerminalUtil.nextLine();
            runWithCommand(command);
            loggerService.command(command);
        } catch (final Exception e) {
            loggerService.error(e);
        }
    }

    protected void runWithCommand(@Nullable String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    public boolean runWithArgument(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        runWithArgument(args[0]);
        return true;
    }

    public void runWithArgument(@Nullable String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(argument);
        abstractCommand.execute();
    }

}
