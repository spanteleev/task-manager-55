package ru.tsc.panteleev.tm.command.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.panteleev.tm.api.service.ICommandService;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.enumerated.Role;

@Getter
@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    public ICommandService commandService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
