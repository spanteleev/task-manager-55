package ru.tsc.panteleev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.panteleev.tm.component.Bootstrap;
import ru.tsc.panteleev.tm.configuration.LoggerConfiguration;

public class Application {

    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run();
    }

}
